<?php

add_action( 'save_post_location', 'acme_save_location' );
/**
 * Hooks into the serialization process of the Location post type 
 * in order to verify the post meta data.
 *
 * @since    1.0.0
 *
 * @param    int    $post_id    The ID of the post type that's being saved'
 */
function acme_save_location( $post_id ) {
  /* If we're not working with the proper post type or the user doesn't have
   * permissions to save, then we'll duck out.
   *
   * Note that the `acme_user_can_save` function has been abstracted in order
   * to consolidate the code. To see more information about this, see
   * this post: https://tommcfarlin.com/save-custom-post-meta-refactored/
   */
  if ( ! acme_user_can_save( $post_id ) ) {
    return;
  }
  
  /* Verify the presence of the plans. If they exist, then save them; otherwise,
   * set an error message to display.
   */
  ( _acme_verify_plans() ) ?
  	acme_save_plan_location( $post_id ) :
  	acme_set_plan_location_error();
  
}

/**
 * Determines whether or not the plan meta data is specified. If it's not, then an error
 * message will be added to the collection of error messages.
 *
 * @since    1.0.0
 * 
 * @return   bool    Whether or not the plan meta data is specified.
 */
function _acme_verify_plans() {
  return empty( $_POST['death_star_plans'] );
}

/**
 * Sanitizes the location of the 'Plan' meta data and then writes it
 * to the database associated with the incoming post.
 *
 * @since    1.0.0
 *
 * @param    int    $post_id    The ID of the current post
 */
function acme_save_plan_location( $post_id ) {
  $location = strip_tags( stripslashes( $_POST['death_star_plans'] ) );
  update_post_meta( $post_id, 'death_star_plans', $location );
  
}

/**
 * Adds an error message to the settings errors if the location of the
 * Death Star plans are not present within the post meta data.
 *
 * @since    1.0.0
 */
function acme_set_plan_location_error() {
 
  add_settings_error(
    'missing-death-star-plans',
    'missing-death-star-plans',
    'You have not specified the location for the Death Star plans.',
    'error'
  );
    
  set_transient( 'settings_errors', get_settings_errors(), 30 );
  
}

add_action( 'admin_notices', '_location_admin_notices' );
/**
 * Writes an error message to the screen if the 'Plan' meta data is not specified for the current
 * post.
 *
 * @since    1.0.0
 */
function _location_admin_notices() {
  // If there are no errors, then we'll exit the function
  if ( ! ( $errors = get_transient( 'settings_errors' ) ) ) {
    return;
  }
  // Otherwise, build the list of errors that exist in the settings errores
  $message = '<div id="acme-message" class="error below-h2"><p><ul>';
  foreach ( $errors as $error ) {
    $message .= '<li>' . $error['message'] . '</li>';
  }
  $message .= '</ul></p></div><!-- #error -->';
  // Write them out to the screen
  echo $message;
  // Clear and the transient and unhook any other notices so we don't see duplicate messages
  delete_transient( 'settings_errors' );
  remove_action( 'admin_notices', '_location_admin_notices' );
}